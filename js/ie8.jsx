// Array
import 'mdn-polyfills/Array.prototype.forEach';
import 'mdn-polyfills/Array.prototype.reduce';
import 'mdn-polyfills/Array.prototype.some';

// Object
import 'mdn-polyfills/Object.create';
import 'mdn-polyfills/Object.keys';

// String
import 'mdn-polyfills/String.prototype.trim';

// Function
import 'mdn-polyfills/Function.prototype.bind';

// Node
import 'mdn-polyfills/Node.prototype.addEventListener';
import 'mdn-polyfills/Node.prototype.children';
import 'mdn-polyfills/Node.prototype.firstElementChild';
