var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: {
    ie: ['./js/ie.jsx'],
    ie8: ['./js/ie8.jsx'],
    map: ['./js/map.jsx'],
  },
  output: {
    path: path.resolve(__dirname, './js/dist'),
  },
  module: {
    rules: [{
      test: /\.jsx$/,
      exclude: [/node_modules\/(?!(swiper|dom7)\/).*/, /\.test\.jsx?$/],
      use: [{
        loader: 'babel-loader',
      }],
    }]
  },
  stats: {
      colors: true
  },
};
